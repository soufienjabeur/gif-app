import React from 'react'
import useGif from '../components/useGif';

const Random_V2 = () => {

    const { gif, fetchGIF } = useGif();
    return (
        <div className="container">
            <h1>Random  Gif</h1>
            <img src={gif} alt="Random Gif " width="500" />
            <button onClick={fetchGIF}>CLICK FOR NEW</button>
        </div>
    )
}

export default Random_V2
