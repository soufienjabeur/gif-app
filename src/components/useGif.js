import { useState, useEffect } from 'react'
import axios from 'axios';


//API_Key
const API_KEY = process.env.REACT_APP_API_KEY;
const URL = `https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}`;

const useGif = (tag) => {

    const [gif, setGif] = useState('')// cause it will be simply an url


    const fetchGIF = async (tag) => {
        const { data } = await axios.get(tag ? `${URL}&tag=${tag}` : URL); //promise  //destructure the data outside the response
        const imgSrc = data.data.images.downsized_large.url;
        setGif(imgSrc);

    }

    //Similare to Component Did Mount ... happens only on the first rendre 'cause the dependecie array is empty
    useEffect(() => {
        fetchGIF(tag);// call the function useEffect hook
    }, [tag]);

    return { gif, fetchGIF };
}

export default useGif;
