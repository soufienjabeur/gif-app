import React, { useState, useEffect } from 'react'
import axios from 'axios';


//API_Key
const API_KEY = process.env.REACT_APP_API_KEY;



const Random_V1 = () => {

    const [gif, setGif] = useState('')// cause it will be simply an url

    const fetchGIF = async () => {
        const URL = `https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}`;
        const { data } = await axios.get(URL) //promise  //destructure the data outside the response

        const imgSrc = data.data.images.downsized_large.url;
        setGif(imgSrc)
        console.log(imgSrc);
        console.log(data);
    }

    //Similare to Component Did Mount ... happens only on the first rendre 'cause the dependecie array is empty
    useEffect(() => {



        fetchGIF();// call the function useEffect hook


    }, []);
    const handleClick = () => {
        fetchGIF();
    }

    return (
        <div className="container">
            <h1>Random Gif</h1>
            <img src={gif} alt="Random Gif " width="500" />
            <button onClick={handleClick}>CLICK FOR NEW</button>
        </div>
    )
}

export default Random_V1
