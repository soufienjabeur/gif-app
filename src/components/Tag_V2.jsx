import React, { useState } from 'react'
import useGif from '../components/useGif';

const Tag_V2 = () => {
    const [tag, setTage] = useState('dogs')//fetch for specific gif
    const { gif, fetchGIF } = useGif(tag);
    return (
        <div className="container">
            <h1>Random {tag}  Gif</h1>
            <img src={gif} alt="Random Gif " width="500" />
            <input type="text" name="tag" value={tag} onChange={(e) => setTage(e.target.value)} />
            <button onClick={()=>fetchGIF(tag)}>CLICK FOR NEW</button>
        </div>
    )
}

export default Tag_V2
