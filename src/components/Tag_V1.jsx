import React, { useState, useEffect } from 'react'
import axios from 'axios';


//API_Key
const API_KEY = process.env.REACT_APP_API_KEY;


const Tag_V1 = () => {


    const [gif, setGif] = useState('')// cause it will be simply an url

    const [tag, setTage] = useState('dogs')//fetch for specific gif

    const fetchGIF = async (tag) => {
        const URL = `https://api.giphy.com/v1/gifs/random?api_key=${API_KEY}&tag=${tag}`;

        const { data } = await axios.get(URL) //promise  //destructure the data outside the response

        const imgSrc = data.data.images.downsized_large.url;
        setGif(imgSrc)
        console.log(imgSrc);
        console.log(data);
    }

    //Similare to Component Did Mount ... happens only on the first rendre 'cause the dependecie array is empty
    useEffect(() => {
        fetchGIF(tag);// call the function useEffect hook
    }, [tag]);
    const handleClick = () => {
        fetchGIF(tag);
    }

    return (
        <div className="container">
            <h1>Random {tag}  Gif</h1>
            <img src={gif} alt="Random Gif " width="500" />
            <input type="text" name="tag" value={tag} onChange={(e) => setTage(e.target.value)} />
            <button onClick={handleClick}>CLICK FOR NEW</button>
        </div>
    )
}

export default Tag_V1
